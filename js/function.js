$(function(){
      // ボタン
    $('#hdr__btn').on('click',function(){
        $(this).children('.hdr__btn-a').toggleClass('btn__a');
        $(this).children('.hdr__btn-b').toggleClass('btn__b');
        $(this).children('.hdr__btn-c').toggleClass('btn__c');

        $('#gnav').toggleClass('gnav-o');
    });

    // var title = document.title;
    // $(document).ready(function(){
    //     if(title = "OnlineStore"){
    //         $('#hdr__btn').hide();
    //         $('#header').css('justify-content', 'flex-start').css('align-items', 'center');
    //         $('.store__ttl').css('letter-spacing', '.3em').css('font-size', '14px').css('margin-left', '14px');
            
    //     }
    // });

    $('#hdr-b').parent().addClass('header-b');
    $('#coffee').parent().addClass('store-main');
    $('#item').parent().addClass('item-main');
    $('.item-main ,.store-main').parent().css('background','#FBF9F7');

      //ナビ スムーススクロール
    $('.store-nav__link[href^="#"]:not([href="#"]), .nav__link[href^="#"]:not([href="#"])').on('click', function(){
        var target = $($(this).attr('href')).offset().top;

        $('html, body').animate({scrollTop: target},500);

        return false;
    });

    var menubg = - $('.shop-menu::after').width();
    $('.shop-menu::after').css('right', menubg);
});